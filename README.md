*Table of Contents*

[[_TOC_]]

# Technical Test v1.1

Sylius version

## The test

As a sylius developer you’re asked to create a newsletter subscription system in Sylius for the
company A. Currently a Customer in Sylius can just check a boolean to receive or not the
newsletters. We would like to extend this and allow our customers to subscribe to the
newsletters they are interested in (“Newsletter 1”, “Newsletter 2”, ...).


As a customer I should be able to choose (check/uncheck) the newsletter I want to read when I
manage my account.


As an administrator, I should be able to manage my newsletters (CRUD) and for each
newsletter see the list of subscribers. (A newsletter has a subject and a content).


A simple but smart (because company A has hundreds of thousands subscribers) command
should be created to send a newsletter to its subscribers (using the newsletter ID as parameter).

The newsletter 1 should contain this :
> Hi {{firstname}},
You are receiving this email because you have subscribed to {{newsletter name}}.
Netenders.


## How we will evaluate you

Criteria that will be evaluated are:
- Good knowledge and usage of Sylius and Symfony
- Reusable code and well factorized
- Simple and concise code
- Good usage of Doctrine and forms.

## How to proceed

The whole test should take about 4h for a senior developer, you return the test once you are
done

## How to return the test

Using GIT, just push your code to a public repository on the platform you are comfortable with
(github, gitlab, bitbucket, ...). Send us an email (or Slack) with the link to the repository

## About the code you write

As coders, the code you write is your intellectual property and belongs to you only; we will not
use it in any matter outside this Technical Test.

# Test resolution

## Course of actions

### Key work
* Set up the project
* Create an entity named **Newsletter**, related to **Customer** with a ManyToMany relation. Fields:
  * Name
  * Subject
  * Body
  * Relation with **Customer** entity
* Create the migration
* Create a fixture for the newsletter
* Create the basic admin crud
* Create the customer form to choose the newsletters
* Create the command to send the newsletters

### Extra work

* Do BDD
* Modify the admin crud with a better selection form for the **Customers** field (show better information, like the email, not the id)
* On the admin crud for the newsletter entity, show a list with the available variables
* Add translation files
* Separate the customer form to manage the newsletters in a new menu

## Problems found

System setup:
* Ubuntu 22.04
* PHP 8.1.5
* Node 16.16.04
* Sylius 1.11.0

During the project setup I found these two problems:

### 'yarn install' failed building node-sass

During the project setup, the yarn install command failed due to make failing to build the node-sass...
I suspect that this may be caused by using PHP 8.1.5, maybe using an older version of PHP can fix it, but I didn't test it.

To fix this I found this issue related to the problem:
* [yarn install fails with error: no template named 'remove_cv_t'](https://github.com/Sylius/Sylius/issues/12893)

And with the fix mentioned in this comment:
* [Sass implementation in Sylius seems to be little outdated...](https://github.com/Sylius/Sylius/issues/12893#issuecomment-898884792)

To fix the issue, it is necessary to modify some vendor files, I thought it would be difficult to test the project.
So I tried with Sylius 1.12 and the docker image, but it seems that the project needs some cooking to be stable.

In the end, I decided to use Sylius 1.11 and the fix mentioned in this section.

### MakerBundle not installed and 'Only attribute mapping is supported by make:entity' error
The latest MakerBundle is only compatible with the new attribute system from PHP 8.

I found this fix to force maker-bundle to generate entities with annotations instead of the attribute system.
Rather than installing the most recent version of MakerBundle, install this version:
> composer req symfony/maker-bundle:1.30.2 --dev

And then follow these instructions:
* [Sass implementation in Sylius seems to be little outdated...](https://github.com/vklux/maker-bundle-force-annotation)

Related issues:
* [Maker Bundle does not work #10809](https://github.com/Sylius/Sylius/issues/10809)
* [Only annotation mapping is supported by make:entity #147](https://github.com/Sylius/SyliusResourceBundle/issues/147)
* [[ERROR] Only annotation mapping is supported by make:entity after upgrading bundles #841](https://github.com/symfony/maker-bundle/issues/841)
