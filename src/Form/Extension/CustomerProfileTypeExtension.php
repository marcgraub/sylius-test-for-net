<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Form\Type\NewsletterChoiceType;
use Sylius\Bundle\CustomerBundle\Form\Type\CustomerProfileType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;

final class CustomerProfileTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('newsletters', NewsletterChoiceType::class, [
                'expanded' => true,
                'multiple' => true,
                'label' => 'app.ui.shop.profile.subscribe_unsubscribe',
            ])
            ->remove('subscribe-newsletter');
    }

    public static function getExtendedTypes(): iterable
    {
        return [CustomerProfileType::class];
    }
}
