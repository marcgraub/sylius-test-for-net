<?php

declare(strict_types=1);

namespace App\Fixture;

use App\Entity\Customer\Customer;
use App\Entity\Customer\Newsletter;
use Doctrine\ORM\EntityManagerInterface;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\CustomerRepository;
use Sylius\Bundle\FixturesBundle\Fixture\AbstractFixture;
use Sylius\Bundle\FixturesBundle\Fixture\FixtureInterface;
use Sylius\Component\Core\Repository\CustomerRepositoryInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

final class NewsletterFixture extends AbstractFixture implements FixtureInterface
{
    private EntityManagerInterface $newsletterManager;
    private CustomerRepository $customerRepository;

    public function __construct(EntityManagerInterface $newsletterManager, CustomerRepositoryInterface $customerRepository)
    {
        $this->newsletterManager = $newsletterManager;
        $this->customerRepository = $customerRepository;
    }

    public function getName(): string
    {
        return 'newsletter';
    }

    public function load(array $options): void
    {
        $newsletter = new Newsletter();
        $newsletter->setName($options['name']);
        $newsletter->setSubject($options['subject']);
        $newsletter->setBody($options['body']);

        $customerList = $this->findCustomers($options['customers']);
        $newsletter = $this->addCustomersToNewsletter($newsletter, $customerList);

        $this->newsletterManager->persist($newsletter);
        $this->newsletterManager->flush();
    }

    private function findCustomers(array $customersIdList): array
    {
        // Would be better to find by email
        $customers = $this->customerRepository->findBy(['id' => $customersIdList]);
        // TODO: check and print a warning if some of the customers are not found
        return $customers;
    }

    private function addCustomersToNewsletter(Newsletter $newsletter, array $customerList): Newsletter
    {
        /* @var Customer $customer */
        foreach ($customerList as $customer) {
            $newsletter->addCustomer($customer);
        }

        return $newsletter;
    }

    protected function configureOptionsNode(ArrayNodeDefinition $optionsNode): void
    {
        $optionsNode
            ->children()
            ->scalarNode('name')->cannotBeEmpty()->end()
            ->scalarNode('subject')->cannotBeEmpty()->end()
            ->scalarNode('body')->cannotBeEmpty()->end()
            ->arrayNode('customers')
                ->performNoDeepMerging()
                ->prototype('scalar')
        ;
    }
}
