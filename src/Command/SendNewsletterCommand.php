<?php

namespace App\Command;

use App\Entity\Customer\Customer;
use App\Entity\Customer\Newsletter;
use App\Repository\Customer\NewsletterRepository;
use Doctrine\Common\Collections\Collection;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\SyntaxError;
use Twig\TemplateWrapper;

class SendNewsletterCommand extends Command
{
    protected static $defaultName = 'app:send-newsletter';
    protected static $defaultDescription = 'Send a newsletter to the customers that are subscribed';
    // TODO: Maybe add the sender email as a field in the channel configuration
    protected static string $defaultSenderEmail = 'send@example.com';

    private NewsletterRepository $newsletterRepository;
    private Swift_Mailer $mailer;
    private Environment $twig;
    private SymfonyStyle $io;

    public function __construct(
        NewsletterRepository $newsletterRepository,
        Swift_Mailer $mailer,
        Environment $twig)
    {
        $this->newsletterRepository = $newsletterRepository;
        $this->mailer = $mailer;
        $this->twig = $twig;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->addArgument('newsletterId', InputArgument::REQUIRED, 'Id to identify a newsletter')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);
        $newsletterId = $input->getArgument('newsletterId');
        $newsletterToSend = $this->newsletterRepository->findOneBy(['id' => $newsletterId]);

        if(empty($newsletterToSend)) {
            $this->io->error('Newsletter not found.');
            return Command::FAILURE;
        }

        $listOfCustomers = $newsletterToSend->getCustomers();

        // TODO: maybe ask for confirmation before sending the newsletters
        // TODO: process the customers with a batch system
        $isProcessSuccessful = $this->processList($listOfCustomers, $newsletterToSend);
        if(!$isProcessSuccessful) {
            return Command::FAILURE;
        }

        $this->io->success('All emails have been sent.');

        return Command::SUCCESS;
    }

    /**
     * @param Collection|array $listOfCustomers
     * @param Newsletter|null $newsletterToSend
     * @return bool
     */
    private function processList(
        Collection|array $listOfCustomers,
        ?Newsletter $newsletterToSend): bool
    {
        try {
            $emailTemplate = $this->twig->createTemplate($newsletterToSend->getBody(), $newsletterToSend->getName());
        } catch (LoaderError | SyntaxError $e) {
            $this->io->error("Error in the email template code. \n\n$e");
            return false;
        }

        /* @var Customer $customer */
        foreach ($listOfCustomers as $customer) {
            $this->io->note('The newsletter has been sent to ' . $customer->getEmail());
            $message = $this->createEmailMessage($newsletterToSend, $customer, $emailTemplate);
            $this->mailer->send($message);
        }

        return true;
    }

    /**
     * @param Newsletter|null $newsletterToSend
     * @param Customer $customer
     * @param TemplateWrapper $emailTemplate
     * @return Swift_Message
     */
    private function createEmailMessage(
        ?Newsletter     $newsletterToSend,
        Customer        $customer,
        TemplateWrapper $emailTemplate): Swift_Message
    {
        return (new Swift_Message($newsletterToSend->getSubject()))
            ->setFrom($this::$defaultSenderEmail)
            ->setTo($customer->getEmail())
            ->setBody(
                $emailTemplate->render(
                    [
                        'firstname' => $customer->getFirstName(),
                        'newsletter_name' => $newsletterToSend->getName(),
                    ]
                ),
                'text/html'
            );
    }
}
