<?php

namespace App\Menu;

use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

final class AdminMenuListener
{
    public function addAdminMenuItems(MenuBuilderEvent $event): void
    {
        $menu = $event->getMenu();

        $newSubmenu = $menu
            ->addChild('customization')
            ->setLabel('Customization')
        ;

        $newSubmenu
            ->addChild('customization-newsletters', [
                'route' => 'app_admin_newsletter_index'
            ])
            ->setLabel('Newsletters')
        ;
    }
}
