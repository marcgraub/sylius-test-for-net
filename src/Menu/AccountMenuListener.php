<?php

namespace App\Menu;

use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

final class AccountMenuListener
{
    public function addShopAccountMenuItems(MenuBuilderEvent $event): void
    {
        $menu = $event->getMenu();

        $newsletterSubmenu = $menu
            ->addChild('newsletters')
//            ->addChild('newsletters', ['route' => ''])
            ->setLabel('Newsletters')
            ->setLabelAttribute('icon', 'inbox')
        ;

    }
}
